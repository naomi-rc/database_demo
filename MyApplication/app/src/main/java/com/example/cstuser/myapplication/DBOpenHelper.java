package com.example.cstuser.myapplication;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by cstuser on 4/6/2018.
 */

public class DBOpenHelper extends SQLiteAssetHelper {
    private static final String DB_NAME = "meetz_database.db";
    private static final int DB_VERSION = 1;

    public DBOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);

    }
}
