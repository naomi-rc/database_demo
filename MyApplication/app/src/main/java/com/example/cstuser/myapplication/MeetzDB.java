package com.example.cstuser.myapplication;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;
import android.widget.Toast;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

/**
 * Created by cstuser on 4/6/2018.
 */

public class MeetzDB {
    private SQLiteOpenHelper openHelper;
    private static MeetzDB instance;
    private SQLiteDatabase database;



    /*This all goes into the OpenHelper and the constructor is renamed after it

    private static final String DB_NAME = "meetz_database";
    private static final int DB_VERSION = 1;

    public MeetzDB(Context context) {
        super(context, DB_NAME, null, DB_VERSION);

    }*/

    //Replace above with this:
    private MeetzDB(Context context) {
        this.openHelper = new DBOpenHelper(context);
    }

    public static MeetzDB getInstance(Context context) {
        if(instance == null) {
            instance = new MeetzDB(context);
            Log.v("TAG", "INSTANCE WAS NULL SO I CREATED A NEW ONE");
        }
        Log.v("TAG", "I AM RETURNING INSTANCE");
        return instance;
    }

    //Include open and close methods for DB
    public void open() {
        Log.v("TAG", "ABOUT TO OPEN DB");
        this.database = openHelper.getWritableDatabase();
        Log.v("TAG", "OPENED DB");
    }
    public void close() {
        if(database != null) {
            this.database.close();
        }
    }


    public String getData() {
        //Define sqlitedatabase and sqlitequerybuilder
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        Log.v("TAG", "CREATED QUERY BUILDER");

        //Define table name and list of rows to query
        String table_name = "Users";
        String[] column_names = {"ID", "Username", "Password", "Email"};

        //Set the tables and the query with the sqlitequerybuilder
        queryBuilder.setTables(table_name);
        Cursor cursor = queryBuilder.query(database, column_names, null, null, null, null, null);
        Log.v("TAG", "SET TABLE AND RETRIEVED CURSOR");


        //Use cursor to get data from the table(s)
        cursor.moveToFirst();

            String data = "";
            int id = 0;
            String username = "";
            String password = "";
            String email = "";
        Log.v("TAG", "MOVED CURSOR TO FIRST");

        //Gather the data FROM EACH ROW with the cursor
            //was using this before: cursor.getColumnIndex(column_names[1])

        while(!cursor.isAfterLast()) {
            //String current_user_data = "";
            id = cursor.getInt(0);
            username = cursor.getString(1);
            password = cursor.getString(2);
            email = cursor.getString(3);

            data += "ID: " + id + " Username: " + username + " Password: " + password + " Email: " + email + "\n";
            cursor.moveToNext();
        }
        Log.v("TAG", "RETRIEVED DATA SUCCESSFULLY");
        cursor.close();
        Log.v("TAG", "RETRIEVED DATA SUCCESSFULLY");
        Log.v("TAG", "DATA IS: " + data);
        return data;

       /*Cursor cursor2 = database.rawQuery("SELECT * FROM Users", null);
        cursor2.moveToFirst();
        while (!cursor2.isAfterLast()) {


            //data += cursor2.getInt(0) + + cursor2.getString"\n";
            cursor2.moveToNext();
        }
        cursor2.close();
        */

//return data;
    }

}
