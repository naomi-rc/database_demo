package com.example.cstuser.myapplication;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private Button btn;
    private Button btnDB;
    private TextView results;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        handler = new Handler();

        results = (TextView)findViewById(R.id.results);

        btn = (Button)findViewById(R.id.notifyMe);
        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                notifyMe();
                results.setText("This is where user data will go");
            }
        });

        btnDB = (Button)findViewById(R.id.readDB);
        btnDB.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "make DB was clicked", Toast.LENGTH_SHORT).show();
                databaseExec();
            }
        });
    }

    public void notifyMe() {
        //Build the notification
        Notification.Builder builder = new Notification.Builder(this, "ID");
        builder
                .setContentTitle("Notification Title")
                .setContentText("This is the notification text")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setAutoCancel(true)
                .setActions();
        Notification notification = builder.build();

        //Create the channel
        NotificationChannel channel = new NotificationChannel("ID", "CHANNEL NAME", NotificationManager.IMPORTANCE_DEFAULT);

        //Get instance of notification service
        NotificationManager manager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        manager.createNotificationChannel(channel);
        manager.notify(1, notification);


    }

    public void databaseExec() {
       MeetzDB databaseAccess = MeetzDB.getInstance(this);
        databaseAccess.open();
        String data = databaseAccess.getData();
        databaseAccess.close();
        results.setText(data);


        /*Needs to be on a new thread > that is why we use access helper

        String text;
        MeetzDB database = new MeetzDB(MainActivity.this);
        String data = database.getData();
        results.setText(data);
        Toast.makeText(MainActivity.this, data, Toast.LENGTH_SHORT).show();
        */

    }

}

